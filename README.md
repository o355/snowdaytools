# Snow Day Tools

Set of web-based tools to help manage the Snow Day API.

# Spaghet-o-meter
Projects made before late 2020 generally include more spaghetti code, and less object-oriented and efficient code. This is a flaw from how I wrote code - usually focusing on development speed rather than future maintainability. Each project I have now gets a rating from 1 to 10 about the spaghettiness of the code.

Snow Day Tools Spaghet-o-meter: **3/10**

The Snow Day Tools has some spaghetti code. Modularity is present in that each tool has a separate file. Less spaghetti can be achieved by the XHR requests for each tool being wrapped into a master function that handled all the requests. The headers for each HTML file are copypasta. Otherwise, the code is pretty readable and you can mostly understand what's going on.

# Getting started
You will need:
* A computer
* (Optional) A web server to host the tools on. You can host them on your local machine if you wish to do so.
* A copy of the Snow Day API for the tools to interact with
* Some modification of the source code to modify which server the tools point at.

The tools are hardcoded to use snowdayapi.owenthe.dev, which if you have your own copy of the Snow Day API won't be the URL of your API. In the JavaScript portion of every file, you'll be able to modify the server name that the tools use.

For each of the tools, the usage is pretty self explanatory. Use an API key, fill out the necessary prefills, and it's good to go.

# License
The Snow Day Tools are licensed under the MIT License.
